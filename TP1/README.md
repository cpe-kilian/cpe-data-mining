# TP1
## EXO4

```python
toutesLesNotes = []
for i in range (5):
    prenom = input("What is your name ? ")
    age = input("How old are you ? ")
    notes=[]
    for j in range (5):
        note = input("What grade have you in module number " + str(j) + " ? ")
        notes.append(note)
    toutesLesNotes.append(notes)

for i in range (5):
    averageGrade = 0
    minimumGrade = toutesLesNotes[i][0]
    maximumGrade = toutesLesNotes[i][0]
    for j in range (5):
        if minimumGrade > toutesLesNotes[j][i]:
            minimumGrade = toutesLesNotes[j][i]
        if maximumGrade < toutesLesNotes[j][i]:
            maximumGrade = toutesLesNotes[j][i]
        averageGrade+=int(toutesLesNotes[j][i])
    print("La note minimale du module " + str(i) + " est : " + str(minimumGrade))
    print("La note maximale du module " + str(i) + " est : " + str(maximumGrade))
    print(str(averageGrade / 5))
```

## EXO5

```python
linesNumber, charsNumber, wordsNumber = 0, 0, 0
wordsList = []

with open("data/page.html", "r") as file:
    
    for line in file:
        linesNumber +=1
        charsNumber += len(line)
        
        words = line.split()
        
        wordsNumber += len(words)
        wordsList.extend(words)
        
print("Lines: "+str(linesNumber))
print("Chars: "+str(charsNumber))
print("Words: "+str(wordsNumber))

# First 20 words
print(wordsList[:20])

# Uniques words
# /!\ Change the order
distinctWords = list(set(wordsList))
print(distinctWords)

print("Uniques words: "+str(len(distinctWords)))
```
from sklearn import datasets
print(datasets.__all__)
```
Lines: 392
Chars: 225702
Words: 5345

['<!DOCTYPE', 'html>', '<!--', 'saved', ...]
['#tsf{margin:0', ...]

Uniques words: 3839
```

## EXO6
```python
year = 0
minimumPop = 600000000
maximumPop = 0
yearMaxPop = 0
yearMinPop = 0
premiereLigne = 1
with open("data/population.csv", "r") as file:
    for line in file:
        x= line.split(",")
        if x[1] != "population\n":
            if int(x[1]) < minimumPop:
                minimumPop = int(x[1])
                yearMinPop = x[0]
            if int(x[1]) > maximumPop:
                maximumPop = int(x[1])
                yearMaxPop = x[0]
        
print("La population maximale " + str(maximumPop) + ", durant l'année " + str(yearMaxPop))
print("La population minimale " + str(minimumPop) + ", durant l'année " + str(yearMinPop))
```
```
La population maximale 66632870, durant l'année 2016
La population minimale 40681000, durant l'année 1901
```

